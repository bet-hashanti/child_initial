package hello;

public class Child {

    public enum Gender {
        Female,
        Male
    }
    public enum House{
        Tel_aviv,
        Bamidbar,
        GeneralHouse
    }
    private final long systemId;
    private String idNumber;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String motherName;
    private String fatherName;
    private House house;

    public Child(long systemId, String idNumber, String firstName,String lastName, Gender gender, String motherName, String fatherName, House house){
        this.systemId = systemId;
        this.idNumber = idNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.motherName = motherName;
        this.fatherName = fatherName;
        this.house = house;

    }

    public long getSystemId(){return this.systemId;}

    public String getIdNumber(){return this.idNumber;}

    public String getfirstName(){return this.firstName;}

    public String getLastName(){return this.lastName;}

    public String getGender(){return this.gender.toString();}

    public String gethouse(){return this.house.toString();}

    public String getMotherName(){return this.motherName;}

    public String getFatherName(){return this.fatherName;}



}

	
